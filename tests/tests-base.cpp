#include "catch2/catch.hpp"

#include "EconomyAI.h"

TEST_CASE_METHOD( economyai::Country, "Create an empty country", "" ) {
    REQUIRE( getName() == "Unknown" );
    REQUIRE( getMoney() == 0 );
}

TEST_CASE_METHOD( economyai::Province, "Create an empty province", "" ) {
    REQUIRE( getName() == "Unknown" );
    REQUIRE( getMoney() == 0 );
}

SCENARIO( "Country parameters changed", "[country]" ) {
    GIVEN( "A country with name" ) {
        std::string name("Entity");
        economyai::Country entity = economyai::Country(name);
        
        REQUIRE( entity.getName() == name );
        REQUIRE( entity.getMoney() == 0 );
        
        WHEN( "change parameters" ) {
            name = "New Name";
            int64 money = 10000000;
            entity.setName(name);
            entity.setMoney(money);
            
            THEN( "parameters really changed" ) {
                REQUIRE( entity.getName() == name );
                REQUIRE( entity.getMoney() == money );
            }
        }
    }
}
