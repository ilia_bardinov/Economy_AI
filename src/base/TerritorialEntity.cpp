#include "TerritorialEntity.h"

namespace economyai
{
    TerritorialEntity::TerritorialEntity(const std::string& _name, const EntityProperties& _properties) : name(_name), properties(_properties)
    {
        //LOG_DEBUG("Create TerritorialEntity - %s", _name.c_str());
    }
    
    TerritorialEntity::TerritorialEntity(const std::string& _name) : name(_name)
    {
        //LOG_DEBUG("Create TerritorialEntity - %s", _name.c_str());
    }
    
    TerritorialEntity::TerritorialEntity()
    {
        //LOG_DEBUG("Create unknown TerritorialEntity");
    }
    
    TerritorialEntity::~TerritorialEntity()
    {
        //LOG_DEBUG("Destroy TerritorialEntity - %s", this->name.c_str());
    }
    
    void TerritorialEntity::update()
    {
        for(TerritorialEntity& subentity : subentities)
        {
            subentity.update();
        }
    }
    
    void TerritorialEntity::parseJSONData(const nlohmann::json& jsonData)
    {
        LOG_DEBUG("Let's parse JSON data for entity \"%s\"", getName().c_str());
        if (jsonData.find("money") != jsonData.end())
        {
            this->setMoney(jsonData["money"]);
        }
        
        if (jsonData.find("Entities") != jsonData.end())
        {
            LOG_DEBUG("Found subentities in \"%s\"!", getName().c_str());
            nlohmann::json jsonDataEntity = jsonData["Entities"];
            for (nlohmann::json::iterator it = jsonDataEntity.begin(); it != jsonDataEntity.end(); ++it) {
                std::string name = it.key();
                TerritorialEntity entity(name);
                entity.parseJSONData(it.value());
                addSubentity(entity);
            }
        }
    }
    
    void TerritorialEntity::setMoney(const int64 _money)
    {
        LOG_DEBUG("Change money in Territorial Entity \"%s\" from %lld to %lld", this->name.c_str(), this->money, _money);
        this->money = _money;
    }
    
    int64 TerritorialEntity::getMoney() const
    {
        return this->money;
    }
    
    void TerritorialEntity::setName(const std::string& _name)
    {
        LOG_DEBUG("Change name in Territorial Entity \"%s\" to \"%s\"", this->name.c_str(), _name.c_str());
        this->name = _name;
    }
    
    std::string TerritorialEntity::getName() const
    {
        return this->name;
    }
    
    EntityProperties& TerritorialEntity::getProperties()
    {
        return properties;
    }
    
    void TerritorialEntity::addSubentity(const TerritorialEntity& entity)
    {
        LOG_DEBUG("Add subentity \"%s\" to entity \"%s\"", entity.getName().c_str(), getName().c_str());
        subentities.push_back(entity);
    }
}
