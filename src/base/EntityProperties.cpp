#include "EntityProperties.h"

namespace economyai
{
    EntityProperties::EntityProperties(const ResourcesMap& _resources) : resources(_resources)
    {
        this->resources = _resources;
    }
    
    EntityProperties::EntityProperties()
    {
        this->resources = ResourcesMap();
    }
    
    void EntityProperties::setResourceValue(EntityResources resourceId, int64 resourceValue)
    {
        resources[resourceId] = resourceValue;
    }
    
    int64 EntityProperties::getResourceValue(EntityResources resourceId)
    {
        return resources[resourceId];
    }
}
