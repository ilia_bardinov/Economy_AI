#include "Country.h"

namespace economyai
{
    Country::Country(const std::string& _name, const EntityProperties& _properties) : TerritorialEntity(_name, _properties)
    {
        LOG_DEBUG("Create Country - %s", _name.c_str());
    }
    
    Country::Country(const std::string& _name) : TerritorialEntity(_name)
    {
        LOG_DEBUG("Create Country - %s", _name.c_str());
    }
    
    Country::Country() : TerritorialEntity()
    {
        LOG_DEBUG("Create unknown Country");
    }
}
