#ifndef ECONOMYAI_PROVINCE_H
#define ECONOMYAI_PROVINCE_H

#include "TerritorialEntity.h"

namespace economyai
{
    class Province : public TerritorialEntity
    {
    public:
        Province(const std::string& name, const EntityProperties& properties);
        Province(const std::string& name);
        Province();        
    };
}

#endif //ECONOMYAI_PROVINCE_H
