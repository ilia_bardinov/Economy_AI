#ifndef ECONOMYAI_TERRITORIAL_ENTITY_H
#define ECONOMYAI_TERRITORIAL_ENTITY_H

#include <string>

#include "define.h"
#include "EntityProperties.h"
#include "macrologger/macrologger.h"
#include "nlohmann/json.hpp"

namespace economyai
{
    class TerritorialEntity
    {
    public:
        TerritorialEntity(const std::string& name, const EntityProperties& properties);
        TerritorialEntity(const std::string& _name);
        TerritorialEntity();
        virtual ~TerritorialEntity();
        
        virtual void parseJSONData(const nlohmann::json& jsonData);
        
        virtual void update();
        
        // setters and getters
        void setMoney(const int64 money);
        int64 getMoney() const;
        void setName(const std::string& name);
        std::string getName() const;
        EntityProperties& getProperties();
        
        void addSubentity(const TerritorialEntity& entity);
        
    private:
        int64 money = 0;
        std::string name = "Unknown";
        EntityProperties properties = EntityProperties();
        
        std::vector<TerritorialEntity> subentities = std::vector<TerritorialEntity>();
    };
}

#endif //ECONOMYAI_TERRITORIAL_ENTITY_H
