#include "Province.h"

namespace economyai
{
    Province::Province(const std::string& _name, const EntityProperties& _properties) : TerritorialEntity(_name, _properties)
    {
        LOG_DEBUG("Create Province - %s", _name.c_str());
    }
    Province::Province(const std::string& _name) : TerritorialEntity(_name)
    {
        LOG_DEBUG("Create Province - %s", _name.c_str());
    }
    
    Province::Province() : TerritorialEntity()
    {
        LOG_DEBUG("Create unknown Province");
    }
}
