#ifndef ECONOMYAI_COUNTRY_H
#define ECONOMYAI_COUNTRY_H

#include <vector>

#include "TerritorialEntity.h"
#include "Province.h"

namespace economyai
{
    class Country : public TerritorialEntity
    {
    public:
        Country(const std::string& name, const EntityProperties& properties);
        Country(const std::string& name);
        Country();                
    };
}

#endif //ECONOMYAI_COUNTRY_H
