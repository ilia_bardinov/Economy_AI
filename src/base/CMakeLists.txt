set(BASE_SOURCES
    ${CMAKE_CURRENT_SOURCE_DIR}/TerritorialEntity.h
    ${CMAKE_CURRENT_SOURCE_DIR}/TerritorialEntity.cpp
    ${CMAKE_CURRENT_SOURCE_DIR}/EntityProperties.h
    ${CMAKE_CURRENT_SOURCE_DIR}/EntityProperties.cpp
    ${CMAKE_CURRENT_SOURCE_DIR}/Province.h
    ${CMAKE_CURRENT_SOURCE_DIR}/Province.cpp
    ${CMAKE_CURRENT_SOURCE_DIR}/Country.cpp
    ${CMAKE_CURRENT_SOURCE_DIR}/Country.h
    ${CMAKE_CURRENT_SOURCE_DIR}/World.cpp
    ${CMAKE_CURRENT_SOURCE_DIR}/World.h
    ${CMAKE_CURRENT_SOURCE_DIR}/EconomyAI.h)

source_group(src FILES ${BASE_SOURCES})

add_library(base STATIC
    ${BASE_SOURCES})

target_link_libraries(base 
    PUBLIC
    common)

target_include_directories(base
  PUBLIC
    # Provide the binary dir for all child targets
    ${CMAKE_BINARY_DIR}
    ${PUBLIC_INCLUDES}
    ${CMAKE_CURRENT_SOURCE_DIR}
  PRIVATE
    ${CMAKE_CURRENT_BINARY_DIR}
)
