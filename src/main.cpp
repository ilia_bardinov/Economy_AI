#include <unistd.h>
#include "EconomyAI.h"

using namespace economyai;

int main(int argc, char** argv)
{
    LOG_INFO("Version: %s", GitRevision::GetFullVersion().c_str());
    
    World::getInstance().init();
    try {
        while(1)
        {
            World::getInstance().update();
            sleep(1);
            break;
        }
    } catch (std::exception) {
        // something
    }

    return 0;
}
