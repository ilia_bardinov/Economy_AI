#include "GitRevision.h"
#include "revision_data.h"

std::string GitRevision::GetHash()
{
    return _HASH;
}

std::string GitRevision::GetDate()
{
    return _DATE;
}

std::string GitRevision::GetBranch()
{
    return _BRANCH;
}

std::string GitRevision::GetCMakeCommand()
{
    return _CMAKE_COMMAND;
}

std::string GitRevision::GetBuildDirectory()
{
    return _BUILD_DIRECTORY;
}

std::string GitRevision::GetSourceDirectory()
{
    return _SOURCE_DIRECTORY;
}

std::string GitRevision::GetFullVersion()
{
    return std::string("Economy AI rev. ") + VER_PRODUCTVERSION_STR;
}

std::string GitRevision::GetCompanyNameStr()
{
    return VER_COMPANYNAME_STR;
}

std::string GitRevision::GetLegalCopyrightStr()
{
    return VER_LEGALCOPYRIGHT_STR;
}

std::string GitRevision::GetFileVersionStr()
{
    return VER_FILEVERSION_STR;
}

std::string GitRevision::GetProductVersionStr()
{
    return VER_PRODUCTVERSION_STR;
}
