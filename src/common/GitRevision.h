#ifndef __GITREVISION_H__
#define __GITREVISION_H__

#include <string>

namespace GitRevision
{
    std::string GetHash();
    std::string GetDate();
    std::string GetBranch();
    std::string GetCMakeCommand();
    std::string GetBuildDirectory();
    std::string GetSourceDirectory();
    std::string GetFullVersion();
    std::string GetCompanyNameStr();
    std::string GetLegalCopyrightStr();
    std::string GetFileVersionStr();
    std::string GetProductVersionStr();
}

#endif
