#ifndef __REVISION_DATA_H__
#define __REVISION_DATA_H__
 #define _HASH                      "@rev_hash@"
 #define _DATE                      "@rev_date@"
 #define _BRANCH                    "@rev_branch@"
 #define _CMAKE_COMMAND             "(@CMAKE_COMMAND@)"
 #define _SOURCE_DIRECTORY          "(@CMAKE_SOURCE_DIR@)"
 #define _BUILD_DIRECTORY           "(@BUILDDIR@)"
 #define VER_COMPANYNAME_STR        "Ilya Bardinov"
 #define VER_LEGALCOPYRIGHT_STR     "(c)2018 Ilya Bardinov"
 #define VER_FILEVERSION            0,0,0
 #define VER_FILEVERSION_STR        "@rev_hash@ @rev_date@ (@rev_branch@ branch)"
 #define VER_PRODUCTVERSION         VER_FILEVERSION
 #define VER_PRODUCTVERSION_STR     VER_FILEVERSION_STR
#endif // __REVISION_DATA_H__