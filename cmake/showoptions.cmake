# output generic information about the core and buildtype chosen
message("")
message("* Economy AI revision   : ${rev_hash} ${rev_date} (${rev_branch} branch)")
if( UNIX )
  message("* Economy AI buildtype  : ${CMAKE_BUILD_TYPE}")
endif()
message("")
message("* Without Git revision  : ${WITHOUT_GIT}")
message("")